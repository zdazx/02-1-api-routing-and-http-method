package com.twuc.webApp.controller;

import com.twuc.webApp.Student;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class BookController {

    // test section 2.1
    @GetMapping("/api/users/{userId}/books")
    public String bookWithId(@PathVariable int userId){
        return "The book for user " + userId;
    }

    // test section 2.3
    @GetMapping("/api/segments/good")
    public String getSegment(){
        return "good";
    }

    @GetMapping("/api/segments/{segmentName}")
    public String getSegmentWithPathVariable(@PathVariable String segmentName){
        return "good_with_variable";
    }

    // test section 2.4
    @GetMapping("/verify/?/signal")
    public String getVerifiedSignal(){
        return "???";
    }

    // test section 2.5
    @GetMapping("/api/wildcards/*")
    public String getSomethingWithString(){
        return "to be verify * in url";
    }

    @GetMapping("/api/wildcard/before/*/after")
    public String getSomethingToVerifyStarSignal(){
        return "start signal in the middle of url";
    }

    // test section 2.6
    @GetMapping("/api/test/*/ab/*/end")
    public String getSomethingToVerifyMoreStartSignal(){
        return "* could be written more in the url";
    }

    // test section 2.7
    @GetMapping("/api/regex/{name:b*}")
    public String getSomethingToVerifyRegex(@PathVariable String name){
        return "regex could be written in the url";
    }

    // test section 2.8
    @GetMapping("/api/students")
    @ResponseBody
    public String getStudentWithGender(@RequestParam String gender, @RequestParam(name = "class") String klass){
        return gender + " + " + klass;
    }

    // exercise of request body and valid in lesson
    @PostMapping("/api/students/all")
    public String getStudentWithName(@RequestBody @Valid Student student){
        return student.getName();
    }

    @PostMapping("/api/students/age")
    public int getStudentWithAge(@RequestBody @Valid Student student){
        return student.getAge();
    }
}
