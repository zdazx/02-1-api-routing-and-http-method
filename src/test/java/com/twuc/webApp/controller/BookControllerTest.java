package com.twuc.webApp.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.Student;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class BookControllerTest {
    private ObjectMapper objectMapper;

    @BeforeEach
    void set_up() {
        objectMapper = new ObjectMapper();
    }

    @Autowired
    private MockMvc mockMvc;

    // test section 2.1
    @Test
    void should_return_book_with_user_id_2() throws Exception {
        mockMvc.perform(get("/api/users/2/books"))
                .andExpect(content().string("The book for user 2"));
    }

    @Test
    void should_return_book_with_user_id_23() throws Exception {
        mockMvc.perform(get("/api/users/23/books"))
                .andExpect(content().string("The book for user 23"));
    }

    // test section 2.3
    @Test
    void should_verify_call_with_good() throws Exception {
        mockMvc.perform(get("/api/segments/good"))
                .andExpect(content().string("good"));
    }

    @Test
    void should_verify_call_with_good_variable() throws Exception {
        mockMvc.perform(get("/api/segments/good"))
                .andExpect(content().string("good"));
    }

    // test section 2.4
    @Test
    void should_verify_signal_with_one_character() throws Exception {
        mockMvc.perform(get("/verify/s/signal"))
                .andExpect(status().is(200));
    }

    @Test
    void should_verify_signal_with_more_character() throws Exception {
        mockMvc.perform(get("verify/sss/signal"))
                .andExpect(status().is(404));
    }

    @Test
    void should_verify_signal_with_no_character() throws Exception {
        mockMvc.perform(get("verify//signal"))
                .andExpect(status().is(404));
    }

    // test section 2.5
    @Test
    void should_match_star_signal() throws Exception {
        mockMvc.perform(get("/api/wildcards/*"))
                .andExpect(status().isOk());
    }

    @Test
    void should_match_star_signal_another() throws Exception {
        mockMvc.perform(get("/api/wildcards/end"))
                .andExpect(status().isOk());
    }

    @Test
    void should_match_start_signal_in_the_middle_url() throws Exception {
        mockMvc.perform(get("/api/wildcard/before/middle/after"))
                .andExpect(status().isOk());
    }

    @Test
    void should_match_start_signal_in_the_middle_url_with_nothing() throws Exception {
        mockMvc.perform(get("/api/wildcard/before/after"))
                .andExpect(status().isNotFound());
    }

    // test section 2.6
    @Test
    void should_verify_some_start_signal_could_be_written_in_the_middle_of_url() throws Exception {
        mockMvc.perform(get("/api/test/before/ab/after/end"))
                .andExpect(status().isOk());
    }

    // test section 2.7
    @Test
    void should_verify_regex_could_be_written_in_the_url() throws Exception {
        mockMvc.perform(get("/api/regex/bbb"))
                .andExpect(status().isOk());
    }

    // test section 2.8
    @Test
    void should_return_female_request_parameter() throws Exception {
        mockMvc.perform(get("/api/students?gender=female&class=A"))
                .andExpect(status().isOk())
                .andExpect(content().string("female + A"));
    }

    @Test
    void should_verify_url_is_working_without_request_param() throws Exception {
        mockMvc.perform(get("/api/students"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_verify_result_when_query_thing_that_should_not_be_present_is_present() throws Exception {
        mockMvc.perform(get("/api/students?gender=female&klass=A"))
                .andExpect(status().isBadRequest());
    }


    // exercise of serialize and request body in lesson
    @Test
    void should_serialize_num() throws JsonProcessingException {
        Integer num = 1;
        String json = objectMapper.writeValueAsString(num);
        assertEquals(json, "1");
    }

    @Test
    void should_deserialize_json() throws IOException {
        String json = "1";
        Integer num = objectMapper.readValue(json, Integer.class);
        assertEquals(num.intValue(), 1);
    }

    @Test
    void should_serialize_student() throws JsonProcessingException {
        Student student = new Student();
        student.setName("xiaoli");
        student.setGender("female");
        student.setAge(20);
        String json = objectMapper.writeValueAsString(student);
        assertEquals(json, "{\"name\":\"xiaoli\",\"gender\":\"female\",\"age\":20,\"klass\":0}");
    }

    @Test
    void should_deserialize_student_json() throws IOException {
        String studentJson = "{\"name\":\"xiaoli\",\"gender\":\"female\"}";
        Student student = objectMapper.readValue(studentJson, Student.class);
        assertEquals(student.getName(),"xiaoli");
        assertEquals(student.getGender(),"female");
    }

    @Test
    void should_return_student_with_name() throws Exception {
        mockMvc.perform(
                post("/api/students/all")
                        .content("{\"name\":\"xiaoli\",\"gender\":\"female\",\"age\":20}")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(content().string("xiaoli"));

//        mockMvc.perform(
//                post("/api/students/all")
//                        .param("name", "xiaoli")
//                        .param("gender", "female")
//                        .contentType(MediaType.APPLICATION_JSON_UTF8)
//        ).andExpect(status().isOk())
//                .andExpect(content().string("xiaoli"));

//        MvcResult mvcResult = mockMvc.perform(
//                post("/api/students/all")
//                        .contentType(MediaType.APPLICATION_JSON_UTF8)
//        ).andReturn();
//        mvcResult.getResponse().getContentAsString()
    }


    @Test
    void should_throw_bad_request_when_name_is_null() throws Exception {
        mockMvc.perform(
                post("/api/students/all")
                        .content("{\"gender\":\"female\"}")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void should_throw_bad_request_when_age_is_not_valid() throws Exception {
        mockMvc.perform(
                post("/api/students/age")
                        .content("{\"name\":\"xiaoli\",\"gender\":\"female\",\"age\":3}")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(status().isBadRequest());
    }
}
